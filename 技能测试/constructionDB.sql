/*===================================*/
/* 3.1 创建数据库constructionDB */
/*===================================*/
CREATE DATABASE [constructionDB] ON  PRIMARY 
( NAME = N'constructionDB', FILENAME = N'C:\Users\Administrator\Desktop\长沙民政职业技术学院_43623223_刘亮_B_1\bak\constructionDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'constructionDB_log', FILENAME = N'C:\Users\Administrator\Desktop\长沙民政职业技术学院_43623223_刘亮_B_1\bak\constructionDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

USE [constructionDB]
GO
/*===================================*/
/* 3.2 创建数据表: T_user */
/*===================================*/
create table T_user(
	user_id char(4) not null primary key,
	user_name char(16) not null,
	user_passwd char(16) not null,
	dept_id char(3),
	Telphone varchar(16),
	address varchar(32),
	handphone varchar(16),
	usb_no varchar(64),
	reserve varchar(64)
)

create table T_func_role_def(
	func_role_id char(3) not null primary key,
	func_role_name char(32) not null,
	reserve varchar(64)
)

create table T_func_item(
	func_id char(4) not null primary key,
	func_name char(32) not null,
	reserve varchar(64)
)

/*===================================*/
/* 3.3 创建数据表间的关系 */
/*===================================*/

create table T_user_role_assign(
	func_role_id char(3) not null,
	user_id char(4) not null,
	primary key (func_role_id, user_id),
	foreign key (user_id) references T_user(user_id),
	foreign key (func_role_id) references T_func_role_def(func_role_id)
)

create table T_role_func_assign(
	func_id char(3) not null,
	func_role_id char(3) not null,
	primary key (func_id, func_role_id),
	foreign key (func_role_id) references T_func_role_def(func_role_id),
	foreign key (func_id) references T_func_item(role_id)
)


