﻿<%@ Page Theme="default" MasterPageFile="~/Master_Page/admin.master" Language="VB" AutoEventWireup="false" CodeFile="class_add.aspx.vb" Inherits="class_add" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="defaultContent">
<div class="nav">
        <a href="#">后台</a> -> <a href="#">系统管理</a> -> 新增班级
    </div>  

    <table class="form-table">
        <tr>
            <th>班级名：</th>
            <td><input type="text" id="name" name="name" runat="server" /></td>
        </tr>
        <tr>
            <th>学院：</th>
            <td><input type="text" id="school" name="school" runat="server" /></td>
        </tr>
        <tr>
            <th>辅导员：</th>
            <td>                   
                <asp:DropDownList ID="DDL_Teacher" runat="server">                    
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <th>备注：</th>
            <td><input type="text" id="reserved" name="reserved" runat="server" /></td>
        </tr>
        <tr>
            <td class="ac" colspan="2">
                <asp:Button class="btn-create" ID="Button1" runat="server" Text="提交" />
            </td>
        </tr>
    </table>
    
</asp:Content>
