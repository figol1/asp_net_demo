﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="menu.aspx.vb" Inherits="template_menu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .menu-frame h3{font-size:18px;}
        .menu-frame h3 li{line-height:20px;margin-bottom:5px;font-size:14px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="menu-frame">
        
        <h3>
            辅导员模块
            <ul>
                <li>历次助学金</li>
                <li>正在进行</li>
                <li>已结束</li>
            </ul>
        </h3>

        <h3>
            学生模块
            <ul>
                <li>历次助学金</li>
                <li>我是候选人</li>
                <li>我的助学金</li>
                <li>我的投票</li>
            </ul>
        </h3>

        <h3>
            用户信息
            <ul>
                <li>个人资料</li>
                <li>修改密码</li>
            </ul>
        </h3>

        <h3>
            系统管理
            <ul>
                <li><a href="#" onclick="parent.document.getElementById('bodyFrame').src='user.aspx'">管理员管理</a></li>
                <li><a href="#" onclick="parent.document.getElementById('bodyFrame').src='teacher.aspx'">辅导员管理</a></li>                
                <li><a href="#" onclick="parent.document.getElementById('bodyFrame').src='class.aspx'">班级管理</a></li>
                <li><a href="#" onclick="parent.document.getElementById('bodyFrame').src='student.aspx'">学生管理</a></li>                
            </ul>
        </h3>   


    </div>
    </form>
</body>
</html>
