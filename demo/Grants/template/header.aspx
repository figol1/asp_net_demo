﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="header.aspx.vb" Inherits="template_header" Theme="default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>页面头部</title>
</head>
<body style="overflow:hidden;">
    <form id="fomr1" runat="server">
    <div class="frame-header">    
        <h3 class="fl">
            <% Response.Write(Session("user_id")) %>
            <%  If Session("user_type") = "student" Then
                    Response.Write("(学生)")
                ElseIf Session("user_type") = "teacher" Then
                    Response.Write("(辅导员)")
                ElseIf Session("user_type") = "admin" Then
                    Response.Write("(管理员)")
                End If
            %>
            你好，欢迎来到助学金评选系统
        </h3>
        <span class="fr">
            <asp:LinkButton ID="LinkButton1" runat="server">注销</asp:LinkButton>
        </span>
    </div>
    </form>
</body>
</html>
