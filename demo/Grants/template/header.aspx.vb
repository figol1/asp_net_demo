﻿
Partial Class template_header
    Inherits System.Web.UI.Page

    Protected Sub Page_Load() Handles Me.Load

        '判断用户是否登陆
        If Session("user_id") Is Nothing Then
            Response.Redirect("../login.aspx") '跳转到登陆界面
        End If

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ' 用户执行注销
        Session.Abandon()
        Response.Write("<script type=""text/javascript"">window.parent.location.href=""../Login.aspx"";</script>")
    End Sub

End Class
