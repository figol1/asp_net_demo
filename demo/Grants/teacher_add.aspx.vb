﻿
Partial Class teacher_add
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '判断用户是否登陆
        If Session("user_id") Is Nothing Then
            '跳转到登陆界面
            Response.Write("<script type=""text/javascript"">window.parent.location.href=""login.aspx"";</script>")
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim u_name As String = name.Value
        Dim u_school As String = school.Value
        Dim u_gender As String = IIf(gender1.Checked = True, "男", "女")
        Dim u_phone As String = phone.Value
        Dim u_reserve As String = reserved.Value
        Dim u_allow_login As String = IIf(allow_login.Checked = True, "1", "0")

        '连接数据库
        Dim sqlServer = New sqlserver()
        Try
            '绑定数据
            sqlServer.Create_Teacher(u_name, u_school, u_phone, u_gender, u_allow_login, u_reserve)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("<script type=""text/javascript"">alert('操作失败');</script>")
            Exit Sub
        Finally
            sqlServer.close()
        End Try

        Dim str As String = "<script type=""text/javascript"">alert('操作成功');</script>"
        Response.Write(str)
    End Sub
End Class
