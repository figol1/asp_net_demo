﻿Imports System.Data

Partial Class class_add
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt As DataTable

        '判断用户是否登陆
        If Session("user_id") Is Nothing Then
            '跳转到登陆界面
            Response.Write("<script type=""text/javascript"">window.parent.location.href=""login.aspx"";</script>")
        End If

        If Not IsPostBack Then
            '连接数据库
            Dim sqlServer = New sqlserver()
            Try
                '绑定数据
                DDL_Teacher.DataSource = sqlServer.LoadData_Teacher()
                DDL_Teacher.DataTextField = "name"
                DDL_Teacher.DataValueField = "teacher_id"
                DDL_Teacher.DataBind()
            Catch ex As Exception
                Response.Write("<script type=""text/javascript"">alert('操作失败');</script>")
            Finally
                sqlServer.close()
            End Try
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim u_name As String = name.Value
        Dim u_school As String = school.Value
        Dim u_teacher As String = DDL_Teacher.SelectedValue
        Dim u_reserve As String = reserved.Value

        '连接数据库
        Dim sqlServer = New sqlserver()
        Try
            '绑定数据
            sqlServer.Create_Class(u_name, u_school, u_teacher, u_reserve)
        Catch ex As Exception
            Response.Write(ex.Message)
            Response.Write("<script type=""text/javascript"">alert('操作失败');</script>")
            Exit Sub
        Finally
            sqlServer.close()
        End Try

        Dim str As String = "<script type=""text/javascript"">alert('操作成功');</script>"
        Response.Write(str)
    End Sub
End Class
