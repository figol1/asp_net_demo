﻿<%@ Page Theme="default" MasterPageFile="~/Master_Page/admin.master" Language="VB" AutoEventWireup="false" CodeFile="class.aspx.vb" Inherits="_class" %>

<asp:Content runat="server" ContentPlaceHolderID="defaultContent">

    <div class="nav">
        <a href="#">后台</a> -> <a href="#">系统管理</a> -> 班级管理
        <a href="class_add.aspx" style="margin-right:20px;" class="fr">新增班级</a>
    </div>
     <asp:ListView ID="ListView1" runat="server" ItemContainerID="ItemPlaceHolder">
        <LayoutTemplate>
            <table class="default-table">
                <thead>
                    <tr>
                        <th>班级ID</th><th>班级名称</th><th>辅导员</th><th>学院</th><th>备注</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <asp:DataPager ID="DataPager1" 
                                            runat="server" 
                                            PagedControlID="ListView1" 
                                            PageSize="10">
                                <Fields>                    
                                    <asp:NextPreviousPagerField ShowNextPageButton="False" ShowFirstPageButton="True" ButtonType="Button" />                    
                                    <asp:NumericPagerField ButtonCount="6" />
                                    <asp:NextPreviousPagerField ShowPreviousPageButton="False" ShowLastPageButton="True" ButtonType="Button" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("class_id")%></td>
                <td><%# Eval("name")%></td>
                <td><%# Eval("teacher_name")%></td>
                <td><%# Eval("school")%></td>
                <td><%# Eval("reserve")%></td>
                <td>
                    <asp:Button ID="DeleteButton" runat="server" 
                        OnClientClick="javascript:confirm('确定要删除该条数据吗？');"
                        CommandName="Delete" Text="删除" 
                        CommandArgument='<%# Eval("class_id")%>' /> 
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <table class="default-table">
                <thead>
                    <tr>
                        <th>班级ID</th><th>班级名称</th><th>辅导员</th><th>学院</th><th>备注</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="ac" colspan="6">目前没有数据</td>
                    </tr>
                </tbody>
            </table>
        </EmptyDataTemplate>
    </asp:ListView>      


</asp:Content>
