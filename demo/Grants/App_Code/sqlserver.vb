﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class sqlserver

    Private database As String = "test"
    Private host As String = "." 'localhost
    Private conn As SqlConnection

    '构造方法，连接到数据库
    Public Sub New()
        Dim connStr As String
        connStr = "Server=" + host + ";Integrated Security=SSPI;Initial Catalog=" + database
        conn = New SqlConnection(connStr)
        conn.Open()
    End Sub

    '删除班级信息
    Public Sub Delete_Class(ByVal class_id As Integer)
        Dim sqlStr As String = "delete from T_class where class_id = " + CStr(class_id)
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    '增加班级
    Public Sub Create_Class(ByVal name As String, ByVal school As String,
                                ByVal teacher_id As String, ByVal reserve As String)
        Dim sqlStr As String = "insert into T_class " +
                                "(name, school, teacher_id, reserve) values " +
                                "('" + name + "', '" + school + "', '" +
                                teacher_id + "', '" + reserve + "')"
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    '加载班级数据
    Public Function LoadData_Class() As DataTable
        '创建查询语句
        Dim sql As String = "SELECT TC.*, TT.name AS teacher_name FROM T_class TC " +
                            "LEFT JOIN T_teacher TT " +
                            "ON TC.teacher_id = TT.teacher_id"

        '执行查询，返回查询结果
        Dim cmd As New SqlCommand(sql, conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        Dim dt As DataTable = New DataTable()
        dt.Load(dr)
        Return dt
    End Function

    '删除辅导员员用户
    Public Sub Delete_Teacher(ByVal teacher_id As Integer)
        Dim sqlStr As String = "delete from T_teacher where teacher_id = " + CStr(teacher_id)
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    Public Sub Create_Teacher(ByVal name As String, ByVal school As String,
                                ByVal phone As String, ByVal gender As String,
                                ByVal allow_login As String, ByVal reserve As String)
        Dim sqlStr As String = "insert into T_teacher " +
                                "(name, school, phone, gender, allow_login, reserve) values " +
                                "('" + name + "', '" + school + "', '" +
                                phone + "', '" + gender + "', '" +
                                allow_login + "', '" + reserve + "')"
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    '加载辅导员数据
    Public Function LoadData_Teacher() As DataTable
        '创建查询语句
        Dim sql As String = "select * from T_teacher;"

        '执行查询，返回查询结果
        Dim cmd As New SqlCommand(sql, conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        Dim dt As DataTable = New DataTable()
        dt.Load(dr)
        Return dt
    End Function

    Public Sub Create_Admin(ByVal name As String, ByVal school As String, ByVal allow_login As String, ByVal reserve As String)
        Dim sqlStr As String = "insert into T_admin " +
                                "(name, school, allow_login, reserve) values " +
                                "('" + name + "', '" + school + "', '" + allow_login + "', '" + reserve + "')"
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    '删除管理员用户
    Public Sub Delete_Admin(ByVal admin_id As Integer)
        Dim sqlStr As String = "delete from T_admin where admin_id = " + CStr(admin_id)
        Dim cmd As New SqlCommand(sqlStr, conn)
        cmd.ExecuteNonQuery()
    End Sub

    '加载管理员数据
    Public Function LoadData_Admin() As DataTable
        '创建查询语句
        Dim sql As String = "select * from T_admin;"

        '执行查询，返回查询结果
        Dim cmd As New SqlCommand(sql, conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        Dim dt As DataTable = New DataTable()
        dt.Load(dr)
        Return dt
    End Function

    '判断用户名、密码、用户类型 是否匹配
    '返回 True / False
    Public Function Is_User_Matched(ByVal user_name As String, ByVal user_passwd As String, ByVal type As String) As Boolean
        '通过用户类型参数，来映射 用户对应的table
        Dim tab_name As String = getTabName(type)

        '创建查询语句
        Dim sql As String = "select name from " + tab_name + " where " +
                            "allow_login = '1' and " +
                            "name = '" + user_name + "' and " +
                            "password = '" + user_passwd + "'"

        '执行查询，如果匹配则返回True，否则返回False
        Dim cmd As New SqlCommand(sql, conn)
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        If dr.HasRows Then
            Return True
        Else
            Return False
        End If
    End Function


    '通过用户类型来映射 用户数据表
    Private Function getTabName(ByVal type As String) As String
        If type = "student" Then
            Return "T_student"
        ElseIf type = "teacher" Then
            Return "T_teacher"
        ElseIf type = "admin" Then
            Return "T_admin"
        Else
            Return ""
        End If
    End Function

    Public Sub close()
        If Not conn Is Nothing Then
            conn.Close()
        End If
    End Sub

End Class
