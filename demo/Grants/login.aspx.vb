﻿Imports sqlserver

Partial Class login
    Inherits System.Web.UI.Page

    Protected Sub Login_Check() Handles Me.Load

        '判断用户是否登陆
        If Len(Session("user_id")) > 0 Then
            Response.Redirect("admin.aspx") '跳转到管理界面
        End If

        '只对表单提交时进行数据验证
        If Not Request.Form Is Nothing Then

            '接收用户输入数据
            Dim name As String = Request.Form.Get("user_name")
            Dim passwd As String = Request.Form.Get("user_passwd")
            Dim type As String = Request.Form.Get("type")

            '连接数据库
            Dim sqlServer = New sqlserver()
            Try
                '判断用户数据是否匹配
                If sqlServer.Is_User_Matched(name, passwd, type) Then
                    Session("user_id") = name '登陆成功，设置用户ID
                    Session("user_type") = type '登陆成功，设置用户类型
                    Session.Timeout = 60 'Session过期时间为60分钟
                    Response.Redirect("admin.aspx") '跳转到管理界面
                Else
                    Response.Write("<script type=""text/javascript"">alert('数据不匹配，请重新输入!');</script>")
                End If
            Catch ex As Exception
                'Response.Write("excepiton found")
            Finally
                sqlServer.close()
            End Try

        End If
    End Sub
End Class
