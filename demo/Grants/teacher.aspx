﻿<%@ Page MasterPageFile="~/Master_Page/admin.master" Language="VB" AutoEventWireup="false" CodeFile="teacher.aspx.vb" Inherits="teacher" Theme="default" %>

<asp:Content runat="server" ContentPlaceHolderID="defaultContent">


    <div class="nav">
        <a href="#">后台</a> -> <a href="#">系统管理</a> -> 辅导员管理
        <a href="teacher_add.aspx" style="margin-right:20px;" class="fr">新增辅导员</a>
    </div>
     <asp:ListView ID="ListView1" runat="server" ItemContainerID="ItemPlaceHolder">
        <LayoutTemplate>
            <table class="default-table">
                <thead>
                    <tr>
                        <th>辅导员ID</th><th>用户名</th><th>学院</th><th>允许登陆？</th><th>电话</th><th>性别</th><th>备注</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="8">
                            <asp:DataPager ID="DataPager1" 
                                            runat="server" 
                                            PagedControlID="ListView1" 
                                            PageSize="10">
                                <Fields>                    
                                    <asp:NextPreviousPagerField ShowNextPageButton="False" ShowFirstPageButton="True" ButtonType="Button" />                    
                                    <asp:NumericPagerField ButtonCount="6" />
                                    <asp:NextPreviousPagerField ShowPreviousPageButton="False" ShowLastPageButton="True" ButtonType="Button" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("teacher_id")%></td>
                <td><%# Eval("name")%></td>
                <td><%# Eval("school")%></td>
                <td><%# Eval("allow_login")%></td>
                <td><%# Eval("phone")%></td>
                <td><%# Eval("gender")%></td>
                <td><%# Eval("reserve")%></td>
                <td>
                    <asp:Button ID="DeleteButton" runat="server" 
                        OnClientClick="javascript:confirm('确定要删除该条数据吗？');"
                        CommandName="Delete" Text="删除" 
                        CommandArgument='<%# Eval("teacher_id")%>' /> 
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <table class="default-table">
                <thead>
                    <tr>
                        <th>辅导员ID</th><th>用户名</th><th>学院</th><th>允许登陆？</th><th>电话</th><th>性别</th><th>备注</th><th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="ac" colspan="8">目前没有数据</td>
                    </tr>
                </tbody>
            </table>
        </EmptyDataTemplate>
    </asp:ListView>      


</asp:Content>
