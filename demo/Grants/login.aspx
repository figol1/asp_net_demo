﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="login.aspx.vb" Inherits="login" Theme="default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>用户登陆--助学金评选系统</title>
</head>
<body>
    <form id="form1" runat="server" method="post" action="">
        <div class="frame-login">
            <table>
                <tr>
                    <th>用户名：</th>
                    <td class="al">
                        <input type="text" runat="server" id="user_name" name="user_name" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="user_name" Display="None" 
                            ErrorMessage="用户名不能为空">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <th>密码：</th>
                    <td class="al">
                        <input type="password" runat="server" id="user_passwd" name="user_passwd" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  
                            ControlToValidate="user_passwd" Display="None" 
                            ErrorMessage="密码不能为空">
                        </asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="ac" colspan="2">
                        <input type="radio" value="teacher" name="type" id="Radio1" />
                        <label>辅导员</label>
                        <input type="radio" value="admin" checked="checked" name="type" id="Radio2" />
                        <label>管理员</label>
                        <input type="radio" value="student" name="type" id="Radio3" />
                        <label>学生</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <asp:Button class="btn-login" ID="Button1" runat="server" Text="登陆" />                 
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
                    </td>
                </tr>
            </table>    
        </div>
    </form>
</body>
</html>
