﻿Imports System.Data

Partial Class user
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '判断用户是否登陆
        If Session("user_id") Is Nothing Then
            '跳转到登陆界面
            Response.Write("<script type=""text/javascript"">window.parent.location.href=""login.aspx"";</script>")
        End If

        If Not IsPostBack Then
            '连接数据库
            Dim sqlServer = New sqlserver()
            Try
                '绑定数据
                ListView1.DataSource = sqlServer.LoadData_Admin()
                ListView1.DataBind()
            Catch ex As Exception
                Response.Write("<script type=""text/javascript"">alert('操作失败');</script>")
            Finally
                sqlServer.close()
            End Try
        End If
    End Sub

    Protected Sub ListView1_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles ListView1.ItemDeleting
        Dim lv As ListView = CType(sender, ListView)
        Dim item As ListViewDataItem = lv.Items(e.ItemIndex)
        Dim btn As Button = CType(item.FindControl("DeleteButton"), Button)

        Dim sqlServer = New sqlserver()
        Try
            '删除管理员用户
            sqlServer.Delete_Admin(CInt(btn.CommandArgument))

            '重新绑定数据
            ListView1.DataSource = sqlServer.LoadData_Admin()
            ListView1.DataBind()
        Catch ex As Exception
            Response.Write("<script type=""text/javascript"">alert('操作失败');</script>")
        Finally
            sqlServer.close()
        End Try
        

    End Sub
End Class
