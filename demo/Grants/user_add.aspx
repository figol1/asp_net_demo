﻿<%@ Page Theme="default" MasterPageFile="~/Master_Page/admin.master" Language="VB" AutoEventWireup="false" CodeFile="user_add.aspx.vb" Inherits="admin_add" %>

<asp:Content runat="server" ContentPlaceHolderID="defaultContent">

    <div class="nav">
        <a href="#">后台</a> -> <a href="#">系统管理</a> -> 新增管理员
    </div>  

    <table class="form-table">
        <tr>
            <th>用户名：</th>
            <td><input type="text" id="name" name="name" runat="server" /></td>
        </tr>
        <tr>
            <th>学院：</th>
            <td><input type="text" id="school" name="school" runat="server" /></td>
        </tr>
        <tr>
            <th>允许登陆：</th>
            <td><input type="radio" checked="false" id="allow_login" name="allow_login" runat="server" /></td>
        </tr>
        <tr>
            <th>备注：</th>
            <td><input type="text" id="reserved" name="reserved" runat="server" /></td>
        </tr>
        <tr>
            <td class="ac" colspan="2">
                <asp:Button class="btn-create" ID="Button1" runat="server" Text="提交" />
            </td>
        </tr>
    </table>

</asp:Content>