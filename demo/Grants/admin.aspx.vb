﻿
Partial Class admin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load() Handles Me.Load

        '判断用户是否登陆
        If Session("user_id") Is Nothing Then
            Response.Redirect("login.aspx") '跳转到登陆界面
        End If

    End Sub

End Class
