﻿Public Class WebForm1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack()) Then
            bind()
        End If
    End Sub


    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        bind()
    End Sub

    Protected Sub bind()
        Dim oleDbConnection1 As OleDb.OleDbConnection
        'Dim strConnect As String = "Provider=OraOLEDB.Oracle;Data Source=127.0.0.1/orcl;User ID=scott;Password=admin"
        Dim strConnect As String = "Provider=OraOLEDB.Oracle;Data Source=127.0.0.1/orcl;User ID=scott;Password=admin"
        oleDbConnection1 = New System.Data.OleDb.OleDbConnection(strConnect)

        Dim CommandText As String = "Select * From T_user"
        Dim myCommand As New System.Data.OleDb.OleDbCommand(CommandText, oleDbConnection1)
        Dim myReader As System.Data.OleDb.OleDbDataReader

        oleDbConnection1.Open()
        myReader = myCommand.ExecuteReader()

        'gridview1.datasource = myReader
        'gridview1.databind()
        ListView1.DataSource = myReader
        ListView1.DataBind()

        'If myReader.HasRows Then
        '    Dim sum As Integer
        '    While myReader.Read()
        '        sum += myReader("a")
        '    End While
        '    Label1.Text = sum
        'End If
        myReader.Close()
        oleDbConnection1.Close()

    End Sub

    Protected Sub del(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles ListView1.ItemDeleting


        'Dim lnk As ListViewDataItem = CType(sender, ListView).Items(e.ItemIndex)
        'Dim btn As LinkButton = lnk.FindControl("LinkButton1")
        'Label1.Text = btn.CommandArgument
        Dim lnk As ListViewDataItem = CType(sender, ListView).Items(e.ItemIndex)
        Dim btn As Label = lnk.FindControl("Label2")
        Label1.Text = btn.Text

    End Sub

End Class