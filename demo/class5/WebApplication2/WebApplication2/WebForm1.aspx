﻿<%@ Page Language="vb" EnableEventValidation="true" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="WebApplication2.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button1" runat="server" Text="Button" />
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br />


        <asp:ListView ID="ListView1" ItemPlaceholderID="ItemPlaceHolder" runat="server">
            <LayoutTemplate>
                <table border="2">
                    <thead>
                        <tr>
                            <th>a</th> 
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder runat ="server" 
                            ID="ItemPlaceHolder"></asp:PlaceHolder>
                    </tbody>
                </table>                
            </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                        
                    </td>
                    <td>
                        <asp:LinkButton 
                            CommandName="Delete" 
                            ID="LinkButton1" 
                            runat="server" 
                            CommandArgument='<%# Eval("a")%>'>
                            删除</asp:LinkButton>                        
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    </form>
</body>
</html>
