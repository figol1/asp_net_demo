﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function TextBox1_ClientValidationFunction(source, arguments) {
                arguments.IsValid = false;
        }

// ]]>
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:Label ID="Label1" runat="server" Text="偶数："></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:CustomValidator ID="CustomValidator1" 
            runat="server" 
            ErrorMessage="请输入一个偶数" ClientValidationFunction="TextBox1_ClientValidationFunction" 
            ControlToValidate="TextBox1"></asp:CustomValidator>



    </div>
    </form>
</body>
</html>
