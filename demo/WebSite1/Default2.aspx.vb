﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Default2
    Inherits System.Web.UI.Page

    Private Sub ListView_BindData()
        Dim connStr, sqlStr As String
        connStr = "Server=127.0.0.1;Integrated Security=SSPI;Initial Catalog=test"
        Dim conn As New SqlConnection(connStr)

        'sqlStr = "select top " + CStr(max) + " * from T_user where id not in (select top " + CStr(start) + " id from T_user order by id asc) order by id asc"
        sqlStr = "select * from T_user"
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()

        '如果你在使用ListView控件自定义数据源时使用DataPager进行分页出现
        '“ 具有的数据源必须实现 ICollection，
        '或在 AllowPaging 为 true 的情况下可执行数据源分页。” 
        '这样的错误提示，说明你使用的是datareader作为的数据源;
        '因为DataReader每次只在内存中加载一条数据……；
        '将数据源改为dataset或者datatable即可解决这个问题；
        ' 一、转换成dataset dt.Load(dr) Dim ds As New DataSet()
        ' 二、转换成datatable ds.Tables.Add(dt)  Dim dt As New DataTable()
        ' 三、

        Dim dt As New DataTable()
        dt.Load(dr)
        ListView1.DataSource = dt
        ListView1.DataBind()

        dr.Close()
        conn.Close()
    End Sub

    Protected Sub ListView1_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewDeleteEventArgs) Handles ListView1.ItemDeleting
        Dim lv As ListView = CType(sender, ListView)
        Dim item As ListViewDataItem = lv.Items(e.ItemIndex)
        Dim btn As Button = CType(item.FindControl("DeleteButton"), Button)

        Dim connStr, sqlStr As String
        connStr = "server=.;integrated security=sspi ;initial catalog=test"
        Dim conn As New SqlConnection(connStr)

        sqlStr = "delete from T_user where id = " + btn.CommandArgument
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()

        dr.Close()
        conn.Close()

        ListView_BindData()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ListView_BindData()
        End If
    End Sub

    Protected Sub ListView1_PageChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged
        ListView_BindData()
    End Sub

End Class
