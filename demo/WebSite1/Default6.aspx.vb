﻿Imports System.Data

Partial Class Default6
    Inherits System.Web.UI.Page

    Protected Sub Page_load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Load

        Dim dt As DataTable
        Dim column As DataColumn

        dt = New DataTable()

        column = New DataColumn()
        column.DataType = System.Type.GetType("System.String")
        column.ColumnName = "id"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "name"
        column.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "age"
        column.DataType = System.Type.GetType("System.Int32")
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "phone"
        column.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(column)

        Dim row As DataRow
        For i As Integer = 0 To 40
            row = dt.NewRow()
            row("id") = 10
            row("name") = 10
            row("age") = 10
            row("phone") = 10
            dt.Rows.Add(row)
        Next i

        '给listview绑定datatable
        ListView1.DataSource = dt
        ListView1.DataBind()

    End Sub

End Class
