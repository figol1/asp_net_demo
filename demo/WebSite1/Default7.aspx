﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default7.aspx.vb" Inherits="Default7" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    


        <asp:ListView ItemPlaceholderID="PlaceHolder1" ID="ListView1" runat="server">

            <LayoutTemplate>
                <table border="1" width="500">
                    <thead>
                        <tr>
                            <td>学号</td><td>姓名</td><td>性别</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <asp:DataPager ID="DataPager1" runat="server" PageSize="3">
                                    <Fields>

                                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="False" ShowNextPageButton="False" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ShowLastPageButton="True" ShowPreviousPageButton="False" ButtonType="Button" />
                                    </Fields>
                                </asp:DataPager>     
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#Eval("id") %></td>
                    <td><%#Eval("name") %></td>
                    <td><%# Eval("gender")%></td>
                </tr>
            </ItemTemplate>

        </asp:ListView>    


    </div>
    </form>
</body>
</html>
