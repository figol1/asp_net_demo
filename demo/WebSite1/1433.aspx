﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="1433.aspx.vb" Inherits="_1433" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        
        <asp:ListView ID="ListView1" ItemPlaceholderID="PlaceHolder1" runat="server">

            <LayoutTemplate>
                <table border="1">
                
                    <thead>
                        <tr>
                            <td>学号</td><td>姓名</td><td>邮箱</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>     
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <asp:DataPager PageSize="5"  ID="DataPager1" runat="server">
                                
                                <Fields>
                                    <asp:NextPreviousPagerField ShowFirstPageButton="True" ButtonType="Button" ShowLastPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField  />
                                    <asp:NextPreviousPagerField ShowFirstPageButton="False" ButtonType="Button" ShowLastPageButton="True" ShowNextPageButton="True" ShowPreviousPageButton="False" />

                                </Fields>


                                </asp:DataPager>
                            </td>
                        </tr>
                    </tfoot>
                </table>

            </LayoutTemplate>

            <ItemTemplate>
                <tr>
                    <td><%#Eval("id") %></td>
                    <td><%# Eval("name")%></td>
                    <td><%#Eval("email") %></td>
                </tr>            
            </ItemTemplate>

        </asp:ListView>



    </div>
    </form>
</body>
</html>
