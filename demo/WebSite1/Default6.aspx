﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default6.aspx.vb" Inherits="Default6" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ListView ItemPlaceholderID="PlaceHolder1" ID="ListView1" runat="server">

            <LayoutTemplate>
                <table>
                    <thead>
                        <tr>
                            <td>学号</td>
                            <td>姓名</td>
                            <td>年龄</td>
                            <td>电话</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder  ID="PlaceHolder1" runat="server"></asp:PlaceHolder>         
                    </tbody>
                    <tfoot>
                        <td colspan="4">
                            <asp:DataPager ID="DataPager1" PageSize="5" runat="server">

                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowNextPageButton="False" ShowFirstPageButton="True" />
                                    <asp:NumericPagerField ButtonCount="6" />
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowPreviousPageButton="False" ShowLastPageButton="True" />
                                </Fields>

                            </asp:DataPager>                  
                        </td>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("id") %></td>
                    <td><%# Eval("name")%></td>
                    <td><%# Eval("age")%></td>
                    <td><%# Eval("phone")%></td>
                </tr>
            </ItemTemplate>

        </asp:ListView>
        

    
    </div>
    </form>
</body>
</html>
