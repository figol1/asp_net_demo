﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Default4
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ListView_BindData()
        End If
    End Sub

    Private Sub ListView_BindData()
        Dim connStr, sqlStr As String
        connStr = "server=.;integrated security=sspi ;initial catalog=test"
        Dim conn As New SqlConnection(connStr)

        sqlStr = "select * from T_user"
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()

        Dim dt As New DataTable()
        dt.Load(dr)
        ListView1.DataSource = dt
        ListView1.DataBind()

        dr.Close()
        conn.Close()
    End Sub

    Protected Sub ListView1_ItemCanceling(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCancelEventArgs) Handles ListView1.ItemCanceling
        If e.CancelMode = ListViewCancelMode.CancelingEdit Then
            ListView1.EditIndex = -1
            ListView_BindData()
        ElseIf e.CancelMode = ListViewCancelMode.CancelingInsert Then
            ListView1.InsertItemPosition = InsertItemPosition.None
            ListView_BindData()
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ListView1.InsertItemPosition = InsertItemPosition.LastItem
        ListView_BindData()
    End Sub

    Protected Sub ListView1_Inserting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewInsertEventArgs) Handles ListView1.ItemInserting

        Dim txb1 As TextBox = CType(e.Item.FindControl("TextBox1"), TextBox)
        Dim txb2 As TextBox = CType(e.Item.FindControl("TextBox2"), TextBox)
        Dim txb3 As TextBox = CType(e.Item.FindControl("TextBox3"), TextBox)
        Dim txb4 As TextBox = CType(e.Item.FindControl("TextBox4"), TextBox)

        Dim sqlStr As String
        sqlStr = "'" + txb1.Text + "','" + txb2.Text + "','" + txb3.Text + "','" + txb4.Text + "'"
        sqlStr = "insert into T_user (id, name, age, phone) values (" + sqlStr + ")"

        Dim connStr As String
        connStr = "server=.;integrated security=sspi ;initial catalog=test"
        Dim conn As New SqlConnection(connStr)
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()
        dr.Close()
        conn.Close()

        '重定向到当前页面
        Response.Redirect(Request.Url.ToString())

    End Sub

    Protected Sub ListView1_PageChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged
        ListView_BindData()
    End Sub
End Class
