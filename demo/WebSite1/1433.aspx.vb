﻿Imports System.Data

Partial Class _1433
    Inherits System.Web.UI.Page

    Sub ListView1_DataBind()
        '给ListView1控件绑定数据


        '第一步：创建数据表
        Dim dt As New DataTable()

        Dim column As DataColumn

        column = New DataColumn()
        column.ColumnName = "id"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "name"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "email"
        dt.Columns.Add(column)

        Dim row As DataRow
        For i = 0 To 10
            row = dt.NewRow()
            row("id") = CStr(i + 100)
            row("name") = "刘亮"
            row("email") = "qq@qq.com"
            dt.Rows.Add(row)
        Next i

        '绑定数据
        ListView1.DataSource = dt
        ListView1.DataBind()

    End Sub

    Protected Sub ListView1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Load

        If Not Page.IsPostBack Then

        
            ListView1_DataBind()

        End If



    End Sub

    Protected Sub Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged
        '给ListView1控件绑定数
        ListView1_DataBind()
    End Sub
End Class
