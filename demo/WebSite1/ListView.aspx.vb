﻿Imports System.Data

Partial Class ListView
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Load

        ' 只有页面第一次加载的时候，才给listview绑定数据
        If Not Page.IsPostBack Then
            ListView_BindData()
        End If
    End Sub

    Private Sub ListView_BindData()
        '数据来源：
        '1. 数据库
        '2. 自定义

        Dim dt As DataTable = New DataTable()

        Dim column As DataColumn

        ' 增加列
        column = New DataColumn()
        column.ColumnName = "id"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "name"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "gender"
        dt.Columns.Add(column)

        ' 增加行
        Dim row As DataRow
        For i = 0 To 10
            row = dt.NewRow()
            row("id") = CStr(i + 100)
            row("name") = "刘亮"
            row("gender") = "男"
            dt.Rows.Add(row)
        Next i


        ' 将数据源与数据绑定
        ListView1.DataSource = dt
        ListView1.DataBind()

    End Sub

    Protected Sub ListView1_PagePropertiesChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged

        ' 页码属性发生改变的时候，需要给LISTVIEW重新绑定数据
        ListView_BindData()

    End Sub

End Class
