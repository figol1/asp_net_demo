﻿Imports System.Data

Partial Class Default7
    Inherits System.Web.UI.Page

    Private Sub ListView1_BindData()
        Dim dt As New DataTable()

        Dim column As DataColumn

        '增加列
        column = New DataColumn()
        column.ColumnName = "id"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "name"
        dt.Columns.Add(column)

        column = New DataColumn()
        column.ColumnName = "gender"
        dt.Columns.Add(column)

        '增加数据行
        Dim row As DataRow
        For i = 0 To 10
            row = dt.NewRow()
            row("id") = CStr(100 + i)
            row("name") = "刘亮"
            row("gender") = "男"
            dt.Rows.Add(row)
        Next i

        '绑定数据到listview
        ListView1.DataSource = dt
        ListView1.DataBind()



    End Sub

    Protected Sub ListView1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.Load


        ' Page_Load IsPostBack
        ' 第一次访问此页面才绑定数据
        If Not Page.IsPostBack Then
            ListView1_BindData()
        End If
    End Sub


    Protected Sub ListView1_PageChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged
        ListView1_BindData()
    End Sub
End Class
