﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default4.aspx.vb" Inherits="Default4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <p>
            <asp:Button ID="Button1" runat="server" Text="新增数据" />
        </p>
        <asp:ListView ID="ListView1" runat="server" ItemContainerID="ItemPlaceHolder">
            <InsertItemTemplate>  
                <tr>                  
                    <td>
                        <asp:TextBox ID="TextBox1" size="4" runat="server" Text="" />
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox2" size="4" runat="server" Text="" />
                    </td>
                    <td>
                        <asp:TextBox ID="TextBox3" size="4" runat="server" Text="" />
                    </td>
                    <td>                    
                         <asp:TextBox ID="TextBox4" size="8" runat="server" Text="" />
                    </td>
                    <td>  
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="插入" />  
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="取消" />    
                    </td>  
                </tr>  
            </InsertItemTemplate>  
            <LayoutTemplate>
                <table border="2" width="500">
                    <thead>
                        <tr>
                            <td>学号</td><td>姓名</td><td>年龄</td><td>电话</td><td style="width:100px;"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                <asp:DataPager ID="DataPager1" 
                                                runat="server" 
                                                PagedControlID="ListView1" 
                                                PageSize="5">
                                    <Fields>                    
                                        <asp:NextPreviousPagerField ShowNextPageButton="False" ShowFirstPageButton="True" ButtonType="Button" />                    
                                        <asp:NumericPagerField ButtonCount="6" />
                                        <asp:NextPreviousPagerField ShowPreviousPageButton="False" ShowLastPageButton="True" ButtonType="Button" />
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("id")%></td>
                    <td><%# Eval("name")%></td>
                    <td><%# Eval("age")%></td>
                    <td><%# Eval("phone")%></td>
                    <td></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>        
    </div>
    </form>
</body>
</html>
