﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="Default2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ListView ID="ListView1" runat="server" 
                        ItemContainerID="ItemPlaceHolder">
             <LayoutTemplate>
                <table border="2" width="500">
                    <thead>
                        <tr>
                            <td>学号</td><td>姓名</td><td>年龄</td><td>电话</td><td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder runat="server" ID="ItemPlaceHolder"></asp:PlaceHolder>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">
                                <asp:DataPager ID="DataPager1" 
                                                runat="server" 
                                                PagedControlID="ListView1" 
                                                PageSize="5">
                                    <Fields>                    
                                        <asp:NextPreviousPagerField ShowNextPageButton="False" ShowFirstPageButton="True" ButtonType="Button" />                    
                                        <asp:NumericPagerField ButtonCount="6" />
                                        <asp:NextPreviousPagerField ShowPreviousPageButton="False" ShowLastPageButton="True" ButtonType="Button" />
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("id")%></td>
                    <td><%# Eval("name")%></td>
                    <td><%# Eval("age")%></td>
                    <td><%# Eval("phone")%></td>
                    <td>
                        <asp:Button ID="DeleteButton" runat="server" 
                            CommandName="Delete" Text="删除" 
                            CommandArgument='<%# Eval("id")%>' /> 
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        
    </div>
    </form>
</body>
</html>
