﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ListView.aspx.vb" Inherits="ListView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <!-- 第一步：拖一个ListView数据控件  -->
        <!-- 第二步：定义LayoutTemplate   -->
        <!-- 第三步：定义ItemTemplate  -->
        <!-- 第四步：定义ItemTemplateHolderID  -->
        <asp:ListView ID="ListView1" ItemPlaceholderID="PlaceHolder1" runat="server">

            <LayoutTemplate>
                <table border="1" style="width:500px;">
                    <thead>
                        <tr>
                            <td>学号</td>
                            <td>姓名</td>
                            <td>性别</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                            
                                <asp:DataPager ID="DataPager1" PageSize="5" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ShowFirstPageButton="True" ShowNextPageButton="False" ButtonType="Button" />
                                        <asp:NumericPagerField ButtonCount="5" />
                                        <asp:NextPreviousPagerField ShowLastPageButton="True" ShowPreviousPageButton="False" ButtonType="Button" />
                                    </Fields>
                                </asp:DataPager>
                            
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("id")%></td>
                    <td><%# Eval("name")%></td>
                    <td><%# Eval("gender")%></td>
                </tr>
            </ItemTemplate>

        </asp:ListView>



    </div>
    </form>
</body>
</html>
