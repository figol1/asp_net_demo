﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class Default3
    Inherits System.Web.UI.Page

    Private Sub ListView_BindData()
        Dim connStr, sqlStr As String
        connStr = "server=.;integrated security=sspi ;initial catalog=test"
        Dim conn As New SqlConnection(connStr)

        sqlStr = "select * from T_user"
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()

        Dim dt As New DataTable()
        dt.Load(dr)
        ListView1.DataSource = dt
        ListView1.DataBind()

        dr.Close()
        conn.Close()
    End Sub

    Protected Sub ListView1_Editing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewEditEventArgs) Handles ListView1.ItemEditing
        ListView1.EditIndex = e.NewEditIndex
        ListView_BindData()
    End Sub

    Protected Sub ListView1_ItemCanceling(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewCancelEventArgs) Handles ListView1.ItemCanceling
        If e.CancelMode = ListViewCancelMode.CancelingEdit Then
            ListView1.EditIndex = -1
            ListView_BindData()
        ElseIf e.CancelMode = ListViewCancelMode.CancelingInsert Then
            ListView_BindData()
        End If
    End Sub

    Protected Sub ListView1_ItemUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewUpdateEventArgs) Handles ListView1.ItemUpdating

        Dim lv As ListView = CType(sender, ListView)
        Dim item As ListViewDataItem = lv.Items(e.ItemIndex)

        Dim txb1 As TextBox = CType(item.FindControl("TextBox1"), TextBox)
        Dim hf1 As HiddenField = CType(item.FindControl("HiddenField1"), HiddenField)

        Dim connStr, sqlStr As String
        connStr = "server=.;integrated security=sspi ;initial catalog=test"
        Dim conn As New SqlConnection(connStr)

        sqlStr = "update T_user set phone = '" + txb1.Text + "' where id = " + hf1.Value
        Dim cmd As New SqlCommand(sqlStr, conn)
        conn.Open()
        Dim dr As SqlDataReader = cmd.ExecuteReader()

        dr.Close()
        conn.Close()

        Response.Redirect(Request.Url.ToString())
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ListView_BindData()
        End If
    End Sub

    Protected Sub ListView1_ItemUpdated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ListViewUpdatedEventArgs) Handles ListView1.ItemUpdated
        ListView_BindData()
    End Sub

    Protected Sub ListView1_PageChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListView1.PagePropertiesChanged
        ListView_BindData()
    End Sub
End Class
