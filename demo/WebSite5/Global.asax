﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.ComponentModel.DataAnnotations" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web.DynamicData" %>

<script RunAt="server">
Private Shared s_defaultModel As New MetaModel
Public Shared ReadOnly Property DefaultModel() As MetaModel
    Get
        Return s_defaultModel
    End Get
End Property

Public Shared Sub RegisterRoutes(ByVal routes As RouteCollection)
    '                     重要: 数据模型注册 
    ' 取消对此行的注释，为 ASP.NET Dynamic Data 注册 ADO.NET Entity Framework 模型。
    ' 若要设置 ScaffoldAllTables = true，需符合以下条件，
    ' 即确定希望数据模型中的所有表都支持基架(即模板)。若要控制各个表的
    ' 基架，请为表创建分部类并将
    ' <ScaffoldTable(true)> 特性应用于分部类。
    ' 注意: 请确保将“YourDataContextType”更改为应用程序的数据上下文类的
    ' 名称。
    ' DefaultModel.RegisterContext(GetType(YourDataContextType), New ContextConfiguration() With {.ScaffoldAllTables = False})

    ' 下面的语句支持分页模式，在这种模式下，“列表”、“详细”、“插入” 
    ' 和“更新”任务是使用不同页执行的。若要启用此模式，请取消注释下面 
    ' 的 route 定义，并注释掉后面的 combined-page 模式部分中的 route 定义。
    routes.Add(New DynamicDataRoute("{table}/{action}.aspx") With {
        .Constraints = New RouteValueDictionary(New With {.Action = "List|Details|Edit|Insert"}),
        .Model = DefaultModel})

    ' 下面的语句支持 combined-page 模式，在这种模式下，“列表”、“详细”、“插入”
    ' 和“更新”任务是使用同一页执行的。若要启用此模式，请取消注释下面
    ' 的 routes，并注释掉上面的分页模式部分中的 route 定义。
    'routes.Add(New DynamicDataRoute("{table}/ListDetails.aspx") With {
    '    .Action = PageAction.List,
    '    .ViewName = "ListDetails",
    '    .Model = DefaultModel})

    'routes.Add(New DynamicDataRoute("{table}/ListDetails.aspx") With {
    '    .Action = PageAction.Details,
    '    .ViewName = "ListDetails",
    '    .Model = DefaultModel})
End Sub

Private Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
    RegisterRoutes(RouteTable.Routes)
End Sub

</script>
