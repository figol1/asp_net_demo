﻿Imports System.Web.HttpCookie
Imports System.Web.SessionState.HttpSessionState

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim httpCookie As HttpCookie = Request.Cookies("MyCookie")

        Session("figol") = "liuliang"
        'Session.Remove("figol")

        If Not httpCookie Is Nothing Then
            Response.Write("hello")
            Response.Write(httpCookie("user_name"))
        End If

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim httpCookie As HttpCookie = New HttpCookie("MyCookie")
        httpCookie.Values.Add("user_name", "figol")
        Response.Cookies.Set(httpCookie)
    End Sub

End Class
