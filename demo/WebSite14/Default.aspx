﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" language="javascript">
        function even_check(sender, e) {
            e.IsValid = false;
        }
    </script>


</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="用户："></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" ControlToCompare="TextBox1"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                    runat="server" Display="None"
                    ErrorMessage="用户名不能为空" ControlToValidate="TextBox1">
        </asp:RequiredFieldValidator>   
        
        <br />
        <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                runat="server"  Display="None"
                ErrorMessage="密码不能为空" ControlToValidate="TextBox2">
        </asp:RequiredFieldValidator>

        <br />
        <asp:Button ID="Button1" runat="server" Text="提交" />
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            ShowMessageBox="True" ShowSummary="False" />


        
    </div>
    </form>
</body>
</html>
