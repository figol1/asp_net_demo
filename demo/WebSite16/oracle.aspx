﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="oracle.aspx.vb" Inherits="oracle" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="Button1" runat="server" Text="连接Oracle" />
        <hr />

        <asp:ListView ID="ListView1" ItemPlaceholderID="PlaceHolder1" runat="server">
            <LayoutTemplate>
                <table border="1">
                    <thead>
                        <tr>
                            <td>学号</td><td>姓名</td>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%#Eval("id") %></td>
                    <td><%#Eval("name") %></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

    </div>
    </form>
</body>
</html>
