﻿Imports System.Data.OleDb

Partial Class oracle
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        '第一步：导入OleDb包
        'Imports System.Data.OleDb

        '第二步：设置连接字符串
        'Provider: 数据提供者，默认为：OraOLEDB.Oracle
        'Data Source: host/database name 127.0.0.1/orcl
        'User ID:用户名 scott
        'User Password: 密码 tiger
        Dim connStr As String = "Provider=OraOLEDB.Oracle;Data Source=127.0.0.1/orcl;User ID=scott;Password=tiger"

        '第三步：新建连接
        Dim conn As OleDbConnection
        conn = New OleDbConnection(connStr)

        Dim sql As String = "select * from T_user"
        Dim Command As OleDbCommand
        Command = New OleDbCommand(sql, conn)


        '第四步：打开连接
        conn.Open()

        '第六步：执行命令
        Dim dt As OleDbDataReader
        dt = Command.ExecuteReader()




        '将结果绑定到ListView1上面
        ListView1.DataSource = dt
        ListView1.DataBind()

        '第七步：关闭连接
        conn.Close()

    End Sub


End Class
