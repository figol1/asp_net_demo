﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        '第一步：导入sqllient，sqlclient用来连接sql server
        'Imports System.Data.SqlClient

        '第二步：设置连接字符串
        ' Data Source ： 主机
        ' Initial Catalog ：连接数据库
        ' Integrated Security： Windows 默认身份验证方式连接
        ' User id / User Password : 用户名密码方式连接
        Dim connStr As String
        connStr = "Data Source=127.0.0.1;Initial Catalog=test;Integrated Security=true"

        '第三步：新建连接
        Dim conn As SqlConnection
        conn = New SqlConnection(connStr)

        '第四步：创建命令
        Dim sql As String = "select * from T_user"
        Dim command As SqlCommand
        command = New SqlCommand(sql, conn)

        '第五步: 打开连接|
        conn.Open()

        '第六步：执行命令
        Dim dr As SqlDataReader
        dr = command.ExecuteReader()

        '将DataReader结果集转换成DataTable
        Dim dt As New DataTable()
        dt.Load(dr)

        '将结果绑定到ListView1上面
        ListView1.DataSource = dt
        ListView1.DataBind()

        '第七步：关闭连接
        conn.Close()

    End Sub


End Class
