﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function Button2_onclick() {
            alert('hello');
        }

// ]]>
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>



        <asp:Label ID="Label1" runat="server" Text="用户名："></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator 
            ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="用户名不能为空！" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>

        <br />

        <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label>
        <asp:TextBox type="password" ID="TextBox2" runat="server"></asp:TextBox>
        <br />

        <asp:Label ID="Label3" runat="server" Text="确认密码："></asp:Label>
        <asp:TextBox type="password" ID="TextBox3" runat="server"></asp:TextBox>
        <asp:CompareValidator ID="CompareValidator1" runat="server" 
            ErrorMessage="密码不一致" ControlToCompare="TextBox2" 
            ControlToValidate="TextBox3"></asp:CompareValidator>

    
    </div>
    </form>
</body>
</html>
