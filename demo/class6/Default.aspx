﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin:0 auto;width:420px;text-align:center;">
    
        <h1>用户注册（Web服务器控件）</h1>
        <table border="1">
            <tr>
                <td>姓名：</td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td>密码：</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td>确认密码：</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td>邮箱：</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td>性别：</td>
                <td>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                        <asp:ListItem Value="gentleman" Text="男" />
                        <asp:ListItem Value="lady" Text="女" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td>省份：</td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Value="hunan" Text="湖南省" />
                        <asp:ListItem Value="hubei" Text="湖北省" />
                        <asp:ListItem Value="guangdong" Text="广东省" />
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td>爱好：</td>
                <td>
                    <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                        <asp:ListItem Value="football" Text="足球" />
                        <asp:ListItem Value="basketball" Text="篮球" />
                        <asp:ListItem Value="pingpang" Text="乒乓球" />
                    </asp:CheckBoxList>
                </td>
            </tr>
            
            <tr>
                <td>备注：</td>
                <td>
                    <textarea runat="server" id="TextArea1" rows="2" cols="30"></textarea>
                </td>
            </tr>
            
            <tr>
                <td>上传照片：</td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button1" runat="server" Text="注册" />
                </td>
            </tr>

            <tr style="height:200px;">
                <td id="result" colspan="2" runat="server">
                    
                </td>
            </tr>
        
        </table>




    </div>
    </form>
</body>
</html>
