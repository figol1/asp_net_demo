USE test; 

/* 1. 创建管理员信息表 */
Create Table T_admin(
    admin_id int identity(1,1) Primary key,
    name varchar(32),
    allow_login bit default(1),
    password char(6) default('123456'),
    school varchar(32),
    reserve varchar(32)
)

/* 初始化一个管理员用户 */
insert into T_admin 
	(name, school)
values
	('admin', '软件学院');
	
/* 2. 创建辅导员表 */
Create Table T_teacher(
    teacher_id int identity(1,1) Primary key,
    name varchar(32),
    allow_login bit default(1),
    password char(6) default('123456'),
    phone varchar(11),
    gender char(2) check(gender in('男', '女')),
    school varchar(32),
    reserve varchar(32)
)

/* 3. 创建班级信息表 */
Create Table T_class(
    class_id int identity(1,1) Primary key,
    name varchar(32),
    teacher_id int,
    school varchar(32),
    reserve varchar(32),
    foreign key(teacher_Id) references T_teacher(teacher_id)
)

/* 4. 创建学生信息表 */
Create Table T_student(
    stu_id int identity(1,1) Primary key,    
    class_id int,
    name varchar(32),
    allow_login bit default(1),
    password char(6) default('123456'),
    phone varchar(11),
    gender char(2) check(gender in('男', '女')),
    is_poor char(1) check(is_poor in('1', '0')),
    birthday date,
    ancestral_home varchar(256),
    reserve varchar(32),
    foreign key(class_id) references T_class(class_id)
)

/* 5. 创建助学金评选表 */
Create Table T_grants(
    grt_id int identity(1,1) Primary key,    
    teacher_id int,
    class_id int,
    name varchar(32),
    quota int,
    date_create date,
    reserve varchar(32),
    foreign key(class_id) references T_class(class_id),
    foreign key(teacher_id) references T_teacher(teacher_id)
)

/* 6. 创建助学金评选委员表 */
Create Table T_grants_member(
    mem_id int identity(1,1) Primary key,    
    grt_id int,
    stu_id int,
    reserve varchar(32),
    foreign key(stu_id) references T_student(stu_id),
	foreign key(grt_id) references T_grants(grt_id)
)

/* 7. 创建助学金评选候选人表 */
Create Table T_grants_candidates(
    can_id int identity(1,1) Primary key,    
    grt_id int,
    stu_id int,
    reserve varchar(32),
    foreign key(stu_id) references T_student(stu_id),
	foreign key(grt_id) references T_grants(grt_id)
)

/* 8. 创建助学金评选委员表 */
Create Table T_grants_rewarder(
    rwd_id int identity(1,1) Primary key,    
    grt_id int,
    stu_id int,
    score int,
    reserve varchar(32),
    foreign key(stu_id) references T_student(stu_id),
	foreign key(grt_id) references T_grants(grt_id)
)

/* 9. 创建助学金评选委员表 */
Create Table T_grants_vote(
    vote_id int identity(1,1) Primary key,    
    mem_id int,
    can_id int,
    date_create date,
    reason varchar(32),
    reserve varchar(32),
    foreign key(mem_id) references T_grants_member(mem_id),
	foreign key(can_id) references T_grants_candidates(can_id)
)


