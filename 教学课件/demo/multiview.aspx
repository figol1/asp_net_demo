<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication3._Default" %>  
  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  
<html xmlns="http://www.w3.org/1999/xhtml">  
<head runat="server">  
    <title>用MultiView和View实现选项卡效果</title>  
</head>  
<body>  
    <form id="form1" runat="server">  
    <div>  
        <asp:Button ID="btnIntroduction" runat="server" BorderWidth="0" Text="公司简介" OnClick="btnIntroduction_Click" />  
        <asp:Button ID="btnWelcome" runat="server" BorderWidth="0" Text="欢迎加盟" OnClick="btnIntroduction_Click" />  
        <table style="border: 1px ridge #0000FF; width: 100%;">  
            <tr valign="top">  
                <td style="width: 300; height: 250">  
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">  
                        <asp:View ID="View1" runat="server">  
                            <asp:Label ID="Label1" runat="server"></asp:Label>  
                        </asp:View>  
                        <asp:View ID="View2" runat="server">  
                            <asp:Label ID="Label2" runat="server"></asp:Label>  
                        </asp:View>  
                    </asp:MultiView>  
                </td>  
            </tr>  
        </table>  
    </div>  
    </form>  
</body>  
</html>  