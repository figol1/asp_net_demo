<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>  
    <script type="text/javascript" language="javascript">
        function tt(s, e) {
            e.IsValid = e.Value%2==0?true:false;
        }        
    </script>  
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <h1>必填字段验证：RequiredFieldValidator</h1>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                    runat="server" Display="None"
                                    ErrorMessage="不能为空" 
                                    ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
        <br />

        <h1>比较字段验证：CompareValidator</h1>
        <asp:TextBox ID="TextBox3" type="password" runat="server"></asp:TextBox>
        <asp:TextBox ID="TextBox4" type="password" runat="server"></asp:TextBox>
        
        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None"
            ControlToCompare="TextBox3" ControlToValidate="TextBox4" 
            ErrorMessage="重复密码有误" Operator="Equal"></asp:CompareValidator>

        <br />

        
        <h1>范围验证：RangeValidator</h1>
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:RangeValidator ID="RangeValidator1" runat="server" Display="None"
            ErrorMessage="范围只能在1-10之间" ControlToValidate="TextBox2" MaximumValue="10" 
            MinimumValue="1" Type="Integer"></asp:RangeValidator>
        <br />


        <h1>正则验证：RegularExpressionValidator</h1>
        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
            runat="server" 
            Type="String" Display="None"
            ErrorMessage="请输入有效的邮件地址" ControlToValidate="TextBox5" 
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <br />

        
        <h1>自定义验证：CustomValidator</h1>
        <br />
        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
        <asp:CustomValidator 
            ID="CustomValidator1" 
            runat="server" Display="None"
            ErrorMessage="请您输入一个偶数" 
            ControlToValidate="TextBox6" ClientValidationFunction="tt"></asp:CustomValidator>
    
        <br />


        <asp:Button type="submit" ID="Button1" runat="server" Text="Button" />

        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            BackColor="Yellow" ShowMessageBox="True" ShowSummary="False" />


    </div>
    </form>
</body>
</html>
